# PHP for Drupal

[![pipeline status](https://gitlab.com/florenttorregrosa-docker/images/docker-drupal-php/badges/develop/pipeline.svg)](https://gitlab.com/florenttorregrosa-docker/images/docker-drupal-php/-/commits/develop)

This repo contains the build for the maintained docker container https://hub.docker.com/r/florenttorregrosa/drupal-php.
